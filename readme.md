````
     ██     ██        ██   ██                             
    ████   ░██       ░██  ░░                              
   ██░░██  ░██      ██████ ██  ██████  ███████ 
  ██  ░░██ ░██████ ░░░██░ ░██ ██░░░░██░░██░░░██
 ██████████░██░░░██  ░██  ░██░██   ░██ ░██  ░██
░██░░░░░░██░██  ░██  ░██  ░██░██   ░██ ░██  ░██
░██     ░██░██████   ░░██ ░██░░██████  ███  ░██
░░      ░░ ░░░░░      ░░  ░░  ░░░░░░  ░░░   ░░ 

     ██                     ██                                                 ██    
    ████                   ░░   █████                                         ░██    
   ██░░██    ██████  ██████ ██ ██░░░██ ███████  ██████████   █████  ███████  ██████  
  ██  ░░██  ██░░░░  ██░░░░ ░██░██  ░██░░██░░░██░░██░░██░░██ ██░░░██░░██░░░██░░░██░   
 ██████████░░█████ ░░█████ ░██░░██████ ░██  ░██ ░██ ░██ ░██░███████ ░██  ░██  ░██    
░██░░░░░░██ ░░░░░██ ░░░░░██░██ ░░░░░██ ░██  ░██ ░██ ░██ ░██░██░░░░  ░██  ░██  ░██    
░██     ░██ ██████  ██████ ░██  █████  ███  ░██ ███ ░██ ░██░░██████ ███  ░██  ░░██   
░░      ░░ ░░░░░░  ░░░░░░  ░░  ░░░░░  ░░░   ░░ ░░░  ░░  ░░  ░░░░░░ ░░░   ░░    ░░    

````

### What is this?

A simple scraper + api for the [Copenhagen Airport's arrival/departure tables](https://www.cph.dk), hereafter referred to as *inbound/outbound*.

#### Endpoints 
* http://localhost:3000
* http://localhost:3000/api/combined

These provide less meta-data because they are not directly used in the front-end.

* http://localhost:3000/api/inbound
* http://localhost:3000/api/outbound

#### Running

````
npm install
npm start
````

#### Structure
Typical express.js structure:
````
├── bin
├── public
│   ├── images
│   ├── javascripts
│   └── stylesheets
├── routes
└── views
````

#### Limitations

Works correctly only if both server and browser time are "Europe/Copenhagen"

### Brief
1. Build an API and scrape all flight departures and arrival from
Copenhagen Airport's website: hhttps://www.cph.dk/en/ttps://www.cph.dk/en/

2. Build a simple front-end application where you are able to search on
different parameters such as
- arrivals
- departures
- date and time
- flights from/to a specific city

The assignment can be build in any language you find yourself most
comfortable with or the language you find would support this assignment for
the most valuable output.
