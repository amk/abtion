var fromCalendarElement = document.querySelector("#from");
var tillCalendarElement = document.querySelector("#till");

//initialize calendar pickers:
//https://github.com/bevacqua/rome
rome(fromCalendarElement,{
	initialValue: moment().startOf("day"),
	date: false,
	time: true
}).on('data', function (value) {
  	appVue.$set('from', value)
});

rome(tillCalendarElement,{
	initialValue: moment().endOf("day"),
	date: false,
	time: true
}).on('data', function (value) {
  	appVue.$set('till', value)
});

// workaround to hide time dropdowns, see https://github.com/bevacqua/rome/issues/86
document.querySelector("#from > div > div > div.rd-time-list").style.display = "none";
document.querySelector("#till > div > div > div.rd-time-list").style.display = "none";

//bind dom
var appVue = new Vue({
	el: '#app',
	data: {
		direction: 'both',
		data: [],
    	airport: '',
    	airports: [],
		from: '00:00',
		till: '24:00',
		epochFrom: 0,
		epochTill: 9999999999,
		date: ''
	},
	computed: {
		dates: function(){
			var dates = [
				moment(),
				moment().add(1, 'days'),
				moment().add(2, 'days')
			];

			var friendlyLabels = ['Today', 'Tomorrow', 'Day after'];

			var output = dates.map(function (d,i){
				var o = {}

				if(i==0) o.selected = true
				o.value = d.startOf("day").unix()
				o.friendlyLabel = friendlyLabels[i]
				o.label = d.startOf("day").fromNow()

				return o;
			})

			return output;
		},
    	datehuman: function () {
      		return moment(this.date).format("DD/MM/YY")
    	}
  	},
	methods: {
		updateTimeFilters: function(e){
			console.log("updateTimeFilters",e);
			var timeLow = this.from
			var timeHigh = this.till

			var inputs = [timeLow, timeHigh];
			var outputs = []
			var durationDelta = 0;

			inputs.forEach(function(input){
				var militaryTime = moment(input, "HH:mm");

				var militaryTimeHours = moment.duration(militaryTime.hour(), 'hours');
				var militaryTimeMinutes = moment.duration(militaryTime.minute(), "minutes");

				var duration = moment.duration().add(militaryTimeHours).add(militaryTimeMinutes);
				var absoluteEpoch = moment.unix(appVue.date).add(duration);
				outputs.push(absoluteEpoch.valueOf())

				//Ultra-clever, but what is this good for?
				durationDelta = Math.abs(durationDelta - duration.as("seconds"));
			})
			this.$set('epochFrom', outputs[0]/1000)
			this.$set('epochTill', outputs[1]/1000)
		},
		changeDestination: function(data){
			this.$set('airport', data)
		},
		changeDate: function(event){
			this.load(this.date);
		},
		load: function(date){
			if(!date) date = moment().startOf("day").unix();
			this.$http({url: '/api/combined?date='+date, method: 'GET'}).then(function (response) {
				this.$set('data', response.data)
				this.$set('airports', response.data.meta.airports)
			});
		}
	},
	ready: function(){
		this.load();
	}
})

appVue.$watch('till', function(e){
	appVue.updateTimeFilters(e);
})
appVue.$watch('from', function(e){
	appVue.updateTimeFilters(e);
})

Vue.filter('truncate', function (string, limit){
    if(string.length > limit){
    	return string.substring(0, limit) + '…';
    }else{
    	return string
    }
});

