var express = require('express');
var router = express.Router();
var scrapeIt = require("scrape-it");
var moment = require("moment");
var async = require("async");
var moment = require("moment");
var ColorHash = require("color-hash");
var colorHash = new ColorHash();

router.get('/', function(req, res, next) {
	res.render('index');
});

function doScrape(direction, cb, dateString){ 
	//show less results in debug mode for faster load
	dateString = dateString.format("M/D/YYYY") 

	var debugMode = (process.env.NODE_ENV == "development")? 'False' : 'True';
	direction = (direction == 'inbound')? 'arrivals' : 'departures'

	var baseUrl =  "https://www.cph.dk/en/flight-info/" + direction + "1/?direction=A&\
		showAll=False&\
		keyword=&\
		selectedDate=" + dateString + "&\
		showAllResults=" + debugMode;
	var url = baseUrl.replace(/\t/g,'');

	console.log(" - will scrape from:", url)
	scrapeIt(url, {
		data:{
			listItem: ".info-table tbody tr",
			data:{
				scheduled: ".scheduled-time span:first-child",
				delayed: ".delayed-time",
				airlines: ".airlines",
				destination: ".flight-destin .destination"
			}
		}
	}).then(function(page){
		cb(null, page);
    })
};

router.get('/api/:direction?', function(req, res, next) {
	var direction = req.params.direction
	var date = req.query.date;
	
	//should there be no date provided, scrape for today
	if(!date){
 		date = moment().startOf("day");
	}else{
 		date = moment.unix(date).startOf("day");
	}

	if(direction == 'inbound' || direction == 'outbound'){
		//Handle single-direction api query
		console.log("Will scrape just ", direction);
		doScrape(direction, function(err, results){
			if(err){
				res.status(500).json({ error: 'Scraping error: ' + err });
			}else{
				res.json(results.data);
			}
		});
	}else if(direction == 'combined'){
		console.log("Will scrape both directions")
		//Should there be an /api call - combine both directions in one response
		//We bucket the two results sets in destinations, flatten later
		var combinedResults = {
			inbound: [],
			outbound: []
		}
		
		async.each(['inbound','outbound'], function(what, callback){
			doScrape(what, function(err, results){
				if(err){
					console.log("error in async scraping")
					callback({ error: 'Scraping error: ' + err });
				}else{
					console.log("Returned from scraping", what, "with:", results.data.length);
					combinedResults[what] = results.data;
					callback(null);
				}
			}, date)
		}, function(err){
 			if (err) {
				res.status(500).json({ error: 'Scraping error: ' + err });
    		} else {
				//Would prefer to use lodash, but it's better to show-off
				var output = []
				var airportList = []
				var bothDirections = ['inbound','outbound']

				//Label each entry and push to final array flat
				bothDirections.forEach(function(direction){
					combinedResults[direction].forEach(function(e){
						//Converts AM/PM notation to 24H
						var militaryTime = moment(e.scheduled, "hh:mm a");

						//Extract hours and minutes, make them a single duration
						var militaryTimeHours = moment.duration(militaryTime.hour(), 'hours');
						var militaryTimeMinutes = moment.duration(militaryTime.minute(), "minutes");
						var duration = moment.duration().add(militaryTimeHours).add(militaryTimeMinutes);

						e.militaryTime = militaryTime.format("HH:mm");
						//For verification https://www.tools4noobs.com/online_tools/seconds_to_hh_mm_ss/

						//Label if it's inbound or outbound
						e.direction = direction;

						e.epoch = date.unix() + duration.as('seconds');
						
						//Normalize whitespace in destination
						e.destination = e.destination.replace(/\s+/g, ' ');
						e.destination = e.destination.replace(/^\s+|\s+$/g,''); 

						//Generate a hash per destination
						e.color = colorHash.hex(e.destination);

						airportList.push(e.destination);
						output.push(e)
					})
				})

				airportList = airportList.sort().filter(function(el,i,a) {
        			return (i==a.indexOf(el));
    			});

				output = output.sort(function(a, b) {
					if(a.epoch < b.epoch){
            			return -1;
        			}else if(a.epoch > b.epoch){
            			return 1;
        			}
        			return 0;
				});

				res.json({
					flights: output,
					meta:{
						airports: airportList
					}
				});
    		}
		});
	}else{
		res.status(500).json({ error: 'Non-existing endpoint, try /combined, /inbound or /outbound' });
	}
});

module.exports = router;
